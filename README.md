# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***The [`TTY2MQTT`](https://bitbucket.org/funcodez/funcodes-tty2mqtt) command line tool is a bridge between [`TTY`](https://en.wikipedia.org/wiki/Serial_port) ([`COM`](https://en.wikipedia.org/wiki/Serial_port)) serial communication and an [`MQTT`](https://en.wikipedia.org/wiki/MQTT)  message broker and makes use of the [`REFCODES.ORG`](http://www.refcodes.org/refcodes) artifacts (of the `org.refcodes` group) to provide a [`CLI`](https://en.wikipedia.org/wiki/Command-line_interface) for publishing data from a [`serial port`](https://en.wikipedia.org/wiki/Serial_port) to an [`MQTT`](https://en.wikipedia.org/wiki/MQTT) message broker.***

## Usage ##

See the [`TTY2MQTT`](https://www.metacodes.pro/manpages/tty2mqtt_manpage) manpage for a complete user guide, basic usage instructions can be queried as follows:

```
$ ./tty2mqtt-launcher-x.y.z.sh --help
```

## Downloads ##

For a variety of readily built executables please refer to the [downloads](https://www.metacodes.pro/downloads) section of the [`METACODES.PRO`](https://www.metacodes.pro) site.

## Getting started ##

To get up and running, clone the [`funcodes-tty2mqtt`](https://bitbucket.org/funcodez/funcodes-tty2mqtt/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-tty2mqtt)'s `git` repository.

## How do I get set up? ##

Using `SSH`, go as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`TTY2MQTT`](https://bitbucket.org/funcodez/funcodes-tty2mqtt/) project:

```
git clone git@bitbucket.org:funcodez/funcodes-tty2mqtt.git
```

Using `HTTPS`, go accordingly as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`TTY2MQTT`](https://bitbucket.org/funcodez/funcodes-tty2mqtt/) project:

```
git clone https://bitbucket.org/funcodez/funcodes-tty2mqtt.git
```

Then you can build a [`fat-jar`](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html) and launch the application: 

```
cd funcodes-tty2mqtt
mvn clean install
java -jar target/funcodes-tty2mqtt-0.0.1.jar
```

## Big fat executable bash script (optional) ##

This step is optional, though when running your application under `Linux`, the following will be your friend:

> To build a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script, take a look at the [`scriptify.sh`](https://bitbucket.org/funcodez/funcodes-tty2mqtt/src/master/scriptify.sh) script and the [`build.sh`](https://bitbucket.org/funcodez/funcodes-tty2mqtt/src/master/build.sh) script respectively:

```
./scriptify.sh
./target/tty2mqtt-launcher-x.y.z.sh
```
The resulting `tty2mqtt-launcher-x.y.z.sh` file is a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script being launched via `./target/tty2mqtt-launcher-x.y.z.sh`.

> Building and creating an executable bash script is done by calling `./build.sh`!

## First steps ##

Go for `./target/tty2mqtt-launcher-x.y.z.sh --help` (or `java -jar target/funcodes-tty2mqtt-0.0.1.jar` if you wish) to get instructions on how to invoke the tool.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-tty2mqtt/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
