// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.tty2mqtt;

import static org.refcodes.cli.CliSugar.*;
import static org.refcodes.serial.SerialSugar.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.CharOption;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.DebugFlag;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.NoneOperand;
import org.refcodes.cli.QuietFlag;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.Trap;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.numerical.Endianess;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.serial.AllocSectionDecoratorSegment;
import org.refcodes.serial.AsciizSegment;
import org.refcodes.serial.ByteArraySection;
import org.refcodes.serial.TransmissionException;
import org.refcodes.serial.alt.tty.Parity;
import org.refcodes.serial.alt.tty.StopBits;
import org.refcodes.serial.alt.tty.TtyPort;
import org.refcodes.serial.alt.tty.TtyPortHub;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.SecretHintBuilder;
import org.refcodes.textual.VerboseTextBuilder;

import sun.misc.Unsafe;

/**
 * TTY2MQTT is a bridge between TTY (COM) serial communication and an MQTT
 * message broker and makes use of the REFCODES.ORG artifacts (of the
 * "org.refcodes" group) to implement a command line tool for publishing data
 * from a TTY client to an MQTT server.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * @see "https://stackoverflow.com/questions/46454995/how-to-hide-warning-illegal-reflective-access-in-java-9-without-jvm-argument"
	 */
	static {
		disableIllegalReflectiveAccessWarning();
	}

	private static void disableIllegalReflectiveAccessWarning() {
		try {
			final Field theUnsafeField = Unsafe.class.getDeclaredField( "theUnsafe" );
			theUnsafeField.setAccessible( true );
			final Unsafe theUnsafe = (Unsafe) theUnsafeField.get( null );
			final Class<?> theClass = Class.forName( "jdk.internal.module.IllegalAccessLogger" );
			final Field theLogger = theClass.getDeclaredField( "logger" );
			theUnsafe.putObjectVolatile( theClass, theUnsafe.staticFieldOffset( theLogger ), null );
		}
		catch ( Exception | Error ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private enum PayloadMode {
		BINARY, ASCII
	}

	private static final String NAME = "tty2mqtt";
	private static final String TITLE = "TTY.2.M℺TT";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/tty2mqtt_manpage]";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String DESCRIPTION = "Tool bridging between a serial port and an MQTT message broker for publishing or subscribing messages (see [https://www.metacodes.pro/manpages/tty2mqtt_manpage]).";
	private static final String CONF_PORT = "tty/port";
	private static final String CONF_BAUD = "tty/baud";
	private static final String CONF_DATA_BITS = "tty/dataBits";
	private static final String CONF_STOP_BITS = "tty/stopBits";
	private static final String CONF_PARITY = "tty/parity";
	private static final String CONF_COMMENT_PREFIX = "tty/message/comment/prefix";

	private static final String CONF_MESSAGE_LENGTH_ENDIANESS = "tty/message/length/endianess";
	private static final String CONF_MESSAGE_LENGTH_WIDTH = "tty/message/length/width";
	private static final String CONF_MESSAGE_SUFFIX = "tty/message/suffix";
	private static final String CONF_TOPIC_PREFIX = "tty/message/topic/prefix";
	private static final String CONF_TOPIC_SEPARATOR = "tty/message/topic/separator";
	private static final String CONF_BROKER_URL = "mqtt/broker/url";
	private static final String CONF_BROKER_USER = "mqtt/broker/user";
	private static final String CONF_BROKER_SECRET = "mqtt/broker/secret";
	private static final String CONF_PUBLISHER_ID = "mqtt/publisher/id";
	private static final String CONF_PUBLISHER_RETAINED_FLAG = "mqtt/publisher/retained";
	private static final String CONF_PUBLISHER_QOS_VALUE = "mqtt/publisher/qos";
	private static final String CONF_PUBLISHER_TOPIC = "mqtt/publisher/topic";
	private static final String CONF_SUBSCRIBER_TOPIC = "mqtt/subscriber/topic";
	private static final String CONF_SUBSCRIBER_MODE = "mqtt/subscriber/mode";

	private static final int DEFAULT_QOS = 0;
	private static final boolean DEFAULT_RETAINED_FLAG = false;
	private static final int DEFAULT_BAUD_RATE = 9600;
	private static final int DEFAULT_DATA_BITS = 8;
	private static final String DEFAULT_MESSAGE_SUFFIX = "\n";
	private static final PayloadMode DEFAULT_SUBSCRIBER_MODE = PayloadMode.ASCII;
	private static final int DEFAULT_MESSAGE_LENGTH_WIDTH = 2;
	private static final Endianess DEFAULT_MESSAGE_LENGTH_ENDIANESS = Endianess.LITTLE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the TTY2MQTT main class.
	 * 
	 * @param aProperties The properties as evaluated by the
	 *        {@link #main(String[])} method.
	 */
	public Main( ApplicationProperties aProperties ) throws IOException, MqttException {

		final Boolean isQuiet = aProperties.getBoolean( QuietFlag.ALIAS );
		final Boolean isDebug = aProperties.getBoolean( DebugFlag.ALIAS );
		final Boolean isPublisherRetained = aProperties.getBooleanOr( CONF_PUBLISHER_RETAINED_FLAG, DEFAULT_RETAINED_FLAG );
		final int thePublisherQos = aProperties.getIntOr( CONF_PUBLISHER_QOS_VALUE, DEFAULT_QOS );
		final String thePublisherId = aProperties.get( CONF_PUBLISHER_ID );
		final String thePublisherTopic = aProperties.get( CONF_PUBLISHER_TOPIC );
		final String theSubscriberTopic = aProperties.get( CONF_SUBSCRIBER_TOPIC );
		final int theBaudRate = aProperties.getIntOr( CONF_BAUD, DEFAULT_BAUD_RATE );
		final int theDataBits = aProperties.getIntOr( CONF_DATA_BITS, DEFAULT_DATA_BITS );
		final Parity theParity = aProperties.getEnumOr( Parity.class, CONF_PARITY, Parity.NONE );
		final StopBits theStopBits = aProperties.getEnumOr( StopBits.class, CONF_STOP_BITS, StopBits.AUTO );
		final Character theCommentPrefix = aProperties.getChar( CONF_COMMENT_PREFIX );
		final String theMessageSuffix = aProperties.getOr( CONF_MESSAGE_SUFFIX, DEFAULT_MESSAGE_SUFFIX );
		final Byte theEndOfMessageChar = toEndOfMessageByte( theMessageSuffix );
		final PayloadMode theSubscriberMode = aProperties.getEnumOr( CONF_SUBSCRIBER_MODE, DEFAULT_SUBSCRIBER_MODE );
		final Endianess theMessageLengthEndianess = aProperties.getEnumOr( CONF_MESSAGE_LENGTH_ENDIANESS, DEFAULT_MESSAGE_LENGTH_ENDIANESS );
		final int theMessageLengthWidth = aProperties.getIntOr( CONF_MESSAGE_LENGTH_WIDTH, DEFAULT_MESSAGE_LENGTH_WIDTH );
		final Character theTopicPrefix = aProperties.getChar( CONF_TOPIC_PREFIX );
		final Character theTopicSeparator = aProperties.getChar( CONF_TOPIC_SEPARATOR );
		final String theBrokerUrl = aProperties.get( CONF_BROKER_URL );
		final String theBrokerUser = aProperties.get( CONF_BROKER_USER );
		final String theBrokerSecret = aProperties.get( CONF_BROKER_SECRET );
		final String thePort = aProperties.get( CONF_PORT );

		if ( !isQuiet ) {
			LOGGER.info( CONF_PORT + " = " + thePort );
			LOGGER.info( CONF_BAUD + " = " + theBaudRate );
			LOGGER.info( CONF_DATA_BITS + " = " + theDataBits );
			LOGGER.info( CONF_STOP_BITS + " = " + theStopBits );
			LOGGER.info( CONF_PARITY + " = " + theParity );
			LOGGER.info( CONF_MESSAGE_SUFFIX + " = " + theMessageSuffix );
			LOGGER.info( CONF_MESSAGE_LENGTH_WIDTH + " = " + theMessageLengthWidth );
			LOGGER.info( CONF_MESSAGE_LENGTH_ENDIANESS + " = " + theMessageLengthEndianess );
			LOGGER.info( CONF_COMMENT_PREFIX + " = " + theCommentPrefix );
			LOGGER.info( CONF_TOPIC_PREFIX + " = " + theTopicPrefix );
			LOGGER.info( CONF_TOPIC_SEPARATOR + " = " + theTopicSeparator );
			LOGGER.info( CONF_PUBLISHER_ID + " = " + thePublisherId );
			LOGGER.info( CONF_PUBLISHER_TOPIC + " = " + thePublisherTopic );
			LOGGER.info( CONF_PUBLISHER_RETAINED_FLAG + " = " + isPublisherRetained );
			LOGGER.info( CONF_PUBLISHER_QOS_VALUE + " = " + thePublisherQos );
			LOGGER.info( CONF_SUBSCRIBER_TOPIC + " = " + theSubscriberTopic );
			LOGGER.info( CONF_SUBSCRIBER_MODE + " = " + theSubscriberMode );
			LOGGER.info( CONF_BROKER_URL + " = " + theBrokerUrl );
			LOGGER.info( CONF_BROKER_USER + " = " + ( theBrokerUser != null ? theBrokerUser : "" ) );
			LOGGER.info( CONF_BROKER_SECRET + " = " + ( theBrokerSecret != null ? SecretHintBuilder.asString( theBrokerSecret ) : "" ) );
			LOGGER.printSeparator();
		}

		final TtyPortHub theTtyPortHub = new TtyPortHub();
		try ( TtyPort theTtyPort = theTtyPortHub.toPort( thePort ).withOpen( theBaudRate, theDataBits, theStopBits, theParity ); IMqttClient theMqttClient = new MqttClient( theBrokerUrl, thePublisherId ) ) { // new MqttClient( theBrokerUrl, thePublisherId, null ) 
			final MqttConnectOptions theOptions = new MqttConnectOptions();
			theOptions.setAutomaticReconnect( true );
			theOptions.setCleanSession( true );
			theOptions.setConnectionTimeout( 10 );
			if ( theBrokerUser != null && theBrokerUser.length() != 0 ) {
				theOptions.setUserName( theBrokerUser );
				if ( theBrokerSecret != null && theBrokerSecret.length() != 0 ) {
					theOptions.setPassword( theBrokerSecret.toCharArray() );
				}
			}
			theMqttClient.connect( theOptions );

			final ByteArraySection theSubscribeBinaryPayload = byteArraySection();
			final AllocSectionDecoratorSegment<?> theSubscribeBinaryMessage = allocSegment( theSubscribeBinaryPayload, theMessageLengthWidth, theMessageLengthEndianess );
			final AsciizSegment theSubscribeAsciiMessage = asciizSegment( theEndOfMessageChar );
			theMqttClient.subscribe( theSubscriberTopic, ( topic, msg ) -> {
				final byte[] thePayload = msg.getPayload();
				switch ( theSubscriberMode ) {
				case ASCII -> {
					if ( !isQuiet ) {
						LOGGER.info( "TTY subscriber <" + thePublisherId + "> receives \"" + new String( thePayload, StandardCharsets.US_ASCII ) + "\" from topic [" + theSubscriberTopic + "]..." );
					}
					theSubscribeAsciiMessage.setPayload( thePayload );
					theSubscribeAsciiMessage.transmitTo( theTtyPort );
					if ( LOGGER.isLogInfo() ) {
						LOGGER.printSeparator();
					}
				}
				case BINARY -> {
					theSubscribeBinaryPayload.setPayload( thePayload );
					if ( !isQuiet ) {
						LOGGER.info( "TTY subscriber <" + thePublisherId + "> receives <" + theSubscribeBinaryMessage.getLength() + "> bytes from topic [" + theSubscriberTopic + "]..." );
						LOGGER.info( "Length width is <" + theSubscribeBinaryMessage.getLengthWidth() + "> bytes and payload length is <" + theSubscribeBinaryPayload.getLength() + "> bytes..." );
						if ( LOGGER.isLogInfo() ) {
							LOGGER.printSeparator();
						}
					}
					theSubscribeBinaryMessage.transmitTo( theTtyPort );
				}
				default -> throw new UnhandledEnumBugException( theSubscriberMode );
				}
			} );

			final AsciizSegment thePublishSegment = asciizSegment( theEndOfMessageChar );
			MqttMessage ePublishMqttMessage;
			String ePublishPayload;
			Message ePublishMessage;
			while ( true ) {
				try {
					thePublishSegment.receiveFrom( theTtyPort );
					ePublishPayload = thePublishSegment.getPayload();
					if ( ePublishPayload != null && ePublishPayload.length() != 0 ) {
						if ( theCommentPrefix == null || !theCommentPrefix.equals( ePublishPayload.charAt( 0 ) ) ) {
							try {
								ePublishMessage = new Message( ePublishPayload, theTopicPrefix, theTopicSeparator, thePublisherTopic );
								// No topic in message and no publisher topic configured |-->
								if ( ePublishMessage.topic == null || ePublishMessage.topic.isEmpty() ) {
									LOGGER.info( ePublishPayload );
								}
								// No topic in message and no publisher topic configured <--|
								else {
									ePublishMqttMessage = new MqttMessage( ePublishMessage.message.getBytes() );
									ePublishMqttMessage.setQos( thePublisherQos );
									ePublishMqttMessage.setRetained( isPublisherRetained );
									try {
										theMqttClient.publish( ePublishMessage.topic, ePublishMqttMessage );
										if ( !isQuiet ) {
											LOGGER.info( "TTY publisher <" + thePublisherId + "> published \"" + ePublishMessage.message.trim() + "\" to topic [" + ePublishMessage.topic + "]..." );
										}
									}
									catch ( MqttException e ) {
										LOGGER.error( "TTY publisher <" + thePublisherId + "> cannot publish \"" + ePublishMessage.message.trim() + "\" to topic [" + ePublishMessage.topic + "] as of: " + Trap.asMessage( e ), e );
									}
								}
							}
							catch ( IllegalArgumentException e ) {
								LOGGER.warn( "Skipping payload as of: " + Trap.asMessage( e ), e );
							}
						}
						else {
							// Comment prefix detected in payload |-->
							if ( !isQuiet || isDebug ) {
								LOGGER.info( ePublishPayload );
							}
							// Comment prefix detected in payload <--|
						}
					}
				}
				catch ( TransmissionException e ) {
					LOGGER.warn( e.getMessage() );
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {
		final ConfigOption theConfigArg = configOption();
		final NoneOperand theNoneArg = none( "Uses the default configuration (file)." );
		final IntOption thePublisherQosArg = intOption( "qos", CONF_PUBLISHER_QOS_VALUE, "The QOS (\"Quality-of-Service\") value to use when publishing from TTY to MQTT (defaults to <" + DEFAULT_QOS + ">)." );
		final CharOption theCommentPrefixArg = charOption( "comment-prefix", CONF_COMMENT_PREFIX, "The prefix for data from TTY (COM) serial port to ignore (log with INFO level) and NOT publish via MQTT broker." );
		final CharOption theTopicPrefixArg = charOption( "topic-prefix", CONF_TOPIC_PREFIX, "The prefix identifying a MQTT topic of data from TTY (COM) serial port, the message is published to the MQTT broker with the according topic." );
		final CharOption theTopicSeparatorArg = charOption( "topic-separator", CONF_TOPIC_SEPARATOR, "The character separating the data from TTY (COM) serial port into a MQTT topic and its message." );
		final StringOption theMessageSuffixArg = stringOption( "message-suffix", CONF_MESSAGE_SUFFIX, "End of message character for serial TTY (COM) data, a hex value (e.g. 0x00) denotes the actual value, an '\\n' denotes a newline." );
		final StringOption theSubscriberTopicArg = stringOption( "subscriber-topic", CONF_SUBSCRIBER_TOPIC, "The MQTT topic to subscribe to." );
		final StringOption thePublisherIdArg = stringOption( "publisher-id", CONF_PUBLISHER_ID, "The MQTT publisher ID to use." );
		final StringOption thePublisherTopicArg = stringOption( "publisher-topic", CONF_PUBLISHER_TOPIC, "The (default) MQTT topic to publish to, in case not stated otherwise in the message from TTY (COM) serial port." );
		final StringOption theBrokerUrlArg = stringOption( "broker-url", CONF_BROKER_URL, "The URL of the MQTT broker to connect with." );
		final StringOption theBrokerUserArg = stringOption( "broker-user", CONF_BROKER_USER, "The user name to use when connecting with the MQTT broker." );
		final StringOption theBrokerSecretArg = stringOption( "broker-secret", CONF_BROKER_SECRET, "The user' password to use when connecting with the MQTT broker." );
		final StringOption thePortOption = stringOption( 'p', "port", CONF_PORT, "The COM (serial) port to operate on." );
		final IntOption theBaudOption = intOption( 'b', "baud", CONF_BAUD, "The baud rate to use for the TTY (COM) serial port." );
		final IntOption theDataBitsOption = intOption( "data-bits", CONF_DATA_BITS, "The data bits to use for the TTY (COM) serial port (usually a value of 7 or 8)." );
		final IntOption theMessageLengthWidthOption = intOption( "length-width", CONF_MESSAGE_LENGTH_WIDTH, "The number of bytes to use when prefixing the length of the binary data (usually a value of 1 or 2 denting max. lengths of 256 or 65536 )." );
		final EnumOption<Endianess> theMessageLengthEndianessOption = enumOption( "length-endianess", Endianess.class, CONF_MESSAGE_LENGTH_ENDIANESS, "The endianess to use for the prefixed length of the binary data: " + VerboseTextBuilder.asString( Endianess.values() ) );
		final EnumOption<PayloadMode> theSubscriberModeOption = enumOption( "subscriber-mode", PayloadMode.class, CONF_SUBSCRIBER_MODE, "Denotes the mode of operation, either processing binary or textual data: " + VerboseTextBuilder.asString( PayloadMode.values() ) );
		final EnumOption<Parity> theParityOption = enumOption( "parity", Parity.class, CONF_PARITY, "The parity to use for the TTY (COM) serial port: " + VerboseTextBuilder.asString( Parity.values() ) );
		final EnumOption<StopBits> theStopBitsOption = enumOption( "stop-bits", StopBits.class, CONF_STOP_BITS, "The stop bits to use for the TTY (COM) serial port: " + VerboseTextBuilder.asString( StopBits.values() ) );
		final Flag theInitFlag = initFlag( false );
		final Flag thePublisherRetainedFlag = flag( "retained", CONF_PUBLISHER_RETAINED_FLAG, "The retained flag to use when publishing from TTY to MQTT (defaults to <" + DEFAULT_RETAINED_FLAG + ">)." );
		final Flag theListPortsFlag = flag( 'l', "list", "list", "List all detected TTY (COM) serial ports." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theQuietFlag = quietFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax = cases ( 
			xor( 
				theNoneArg, any( theConfigArg, thePortOption, theBaudOption, theDataBitsOption, theStopBitsOption, theParityOption, theBrokerUrlArg, theMessageSuffixArg, theMessageLengthWidthOption, theMessageLengthEndianessOption, theCommentPrefixArg, theTopicPrefixArg, theTopicSeparatorArg, theSubscriberModeOption,theSubscriberTopicArg, thePublisherIdArg, thePublisherTopicArg, thePublisherRetainedFlag, thePublisherQosArg, theQuietFlag, theDebugFlag, and( theBrokerUserArg, any( theBrokerSecretArg ) ) )
			),
			and( theListPortsFlag, any( theQuietFlag, theDebugFlag )	),
			and( theInitFlag, any( theConfigArg, theQuietFlag, theDebugFlag )),
			xor( theHelpFlag,  and( theSysInfoFlag, any( theQuietFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "List all available TTY (COM) ports", theListPortsFlag ),
			example( "Create an initial configuration file", theInitFlag, theConfigArg ),
			example( "Specific configuration file to use", theConfigArg ),
			example( "Open the TTY (COM) serial port with given settings", thePortOption, theBaudOption, theDataBitsOption, theParityOption ),
			example( "Prefix and separator to parse TTY (COM) serial data", theTopicPrefixArg, theTopicSeparatorArg ),
			example( "Just log TTY (COM) serial data when prefixed", theCommentPrefixArg ),
			example( "Uses the default configuration (file)" ),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();
		try {
			if ( theArgsProperties.getBoolean( theListPortsFlag ) ) {
				listSerialPorts( theQuietFlag.isEnabled() );
				System.exit( 0 );
			}
			new Main( theArgsProperties );
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	/**
	 * List all detected COM (serial) ports.
	 * 
	 * @param isQuiet Be less verbose (no logs).
	 */
	private static void listSerialPorts( boolean isQuiet ) throws IOException {
		final TtyPortHub theTtyPortHub = new TtyPortHub();
		final TtyPort[] thePorts = theTtyPortHub.ports();
		if ( isQuiet ) {
			for ( TtyPort ePort : thePorts ) {
				System.out.println( ePort.getAlias() + "\t" + ePort.getPortMetrics().getBaudRate() + " baud\t" + ePort.getDescription() + "\t" + ePort.getName() );
			}
		}
		else {
			if ( thePorts.length != 0 ) {
				int theMaxWidth = 0;
				int eWidth;
				for ( TtyPort ePort : thePorts ) {
					eWidth = ePort.getAlias().length();
					if ( theMaxWidth < eWidth ) {
						theMaxWidth = eWidth;
					}
				}
				final HorizAlignTextBuilder theBuilder = new HorizAlignTextBuilder().withColumnWidth( theMaxWidth );
				for ( TtyPort ePort : thePorts ) {
					LOGGER.info( "[" + theBuilder.toString( ePort.getAlias() ) + "] " + ePort.getName() + ": \"" + ePort.getDescription() + "\" (" + ePort.getPortMetrics().getBaudRate() + " baud)" );
				}
			}
			else {
				LOGGER.info( "No serial (COM) ports found." );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	static Byte toEndOfMessageByte( String aMessageSuffix ) {
		final String theMessageSuffix = aMessageSuffix.replaceAll( "\\\\n", "\n" ).replaceAll( "\\\\t", "\t" ).replaceAll( "\\\\r", "\r" ).replaceAll( "\\\\f", "\f" ).replaceAll( "\\\\b", "\b" );
		Byte theEndOfMessageChar = null;
		try {
			if ( theMessageSuffix.startsWith( "0x" ) ) {
				theEndOfMessageChar = (byte) Integer.parseInt( theMessageSuffix.substring( 2 ), 16 );
			}
		}
		catch ( Exception ignore ) {}
		if ( theEndOfMessageChar == null && theMessageSuffix.length() == 1 ) {
			theEndOfMessageChar = (byte) theMessageSuffix.charAt( 0 );
		}
		if ( theEndOfMessageChar == null ) {
			throw new IllegalArgumentException( "Cannot convert property '" + CONF_MESSAGE_SUFFIX + "' with value \"" + aMessageSuffix + "\" to valid message suffix char." );
		}
		return theEndOfMessageChar;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	static class Message {

		public String topic = null;
		public String message = null;

		public Message( String aPayload, Character aTopicPrefix, Character aMessageSeparator, String aDefaultTopic ) {
			if ( aTopicPrefix != null && aTopicPrefix.equals( aPayload.charAt( 0 ) ) ) {
				final int eIndex = aPayload.indexOf( aMessageSeparator );
				if ( eIndex == -1 ) {
					throw new IllegalArgumentException( "Unable to distinguish topic using separator '" + aMessageSeparator + "' from payload: \"" + aPayload + "\"." );
				}
				else {
					topic = aPayload.substring( 1, eIndex );
					message = aPayload.substring( eIndex + 1 );
				}
			}
			else {
				topic = aDefaultTopic;
				message = aPayload;
			}
		}
	}
}