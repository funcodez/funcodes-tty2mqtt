module club.funcodes.tty2mqtt {
	requires org.refcodes.cli;
	requires org.eclipse.paho.client.mqttv3;
	requires org.refcodes.archetype;
	requires org.refcodes.serial;
	requires org.refcodes.serial.alt.tty;
	requires jdk.unsupported;
}
