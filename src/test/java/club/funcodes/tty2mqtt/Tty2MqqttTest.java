// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.tty2mqtt;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

import club.funcodes.tty2mqtt.Main.Message;

public class Tty2MqqttTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testMessage1() {
		Message theMessage = new Message( "@topic:message", '@', ':', "defaultTopic" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "topic = " + theMessage.topic );
			System.out.println( "message = " + theMessage.message );
		}
		assertEquals( "topic", theMessage.topic );
		assertEquals( "message", theMessage.message );
	}

	@Test
	public void testMessage2() {
		Message theMessage = new Message( "@topic:", '@', ':', "defaultTopic" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "topic = " + theMessage.topic );
			System.out.println( "message = " + theMessage.message );
		}
		assertEquals( "topic", theMessage.topic );
		assertEquals( "", theMessage.message );
	}

	@Test
	public void testMessage3() {
		Message theMessage = new Message( "message", '@', ':', "defaultTopic" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "topic = " + theMessage.topic );
			System.out.println( "message = " + theMessage.message );
		}
		assertEquals( "defaultTopic", theMessage.topic );
		assertEquals( "message", theMessage.message );
	}

	@Test
	public void testEndOfMessageByte1() {
		byte theEomByte = Main.toEndOfMessageByte( "\\n" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEomByte );
		}
		assertEquals( theEomByte, 10 );
	}

	@Test
	public void testEndOfMessageByte2() {
		byte theEomByte = Main.toEndOfMessageByte( "A" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEomByte );
		}
		assertEquals( theEomByte, 65 );
	}

	@Test
	public void testEndOfMessageByte3() {
		byte theEomByte = Main.toEndOfMessageByte( "0x00" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEomByte );
		}
		assertEquals( theEomByte, 0 );
	}

	@Test
	public void testEndOfMessageByte4() {
		byte theEomByte = Main.toEndOfMessageByte( "0xFF" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEomByte );
		}
		assertEquals( theEomByte, -1 ); // 0xFF = -1 as signed byte 
	}

	@Test
	public void testEndOfMessageByte5() {
		byte theEomByte = Main.toEndOfMessageByte( "0x1F" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEomByte );
		}
		assertEquals( theEomByte, 0x1F );
	}
}
